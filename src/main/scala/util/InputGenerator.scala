package util

import java.io.File

import scala.util.Random


object InputGenerator extends App {

  def generateInput(inputSize: Int = 100000) : IndexedSeq[String] = {
    Range(1, inputSize + 1)
      .map(x => (Random.nextInt(1000000001), Random.nextInt(1000000000) + 1))
      .sortBy(_._1)
      .map(x => s"${x._1} ${x._2}")
  }

  def createInputFile(fileName: String = "/pizza/example00.txt", inputSize: Int = 100000): Unit = {
    def printToFile(f: java.io.File)(op: java.io.PrintWriter => Unit) {
      val p = new java.io.PrintWriter(f)
      try { op(p)} finally {
        p.close()
      }
    }

    printToFile(new File(fileName)) { p =>
      p.println(inputSize)
      generateInput(inputSize).foreach(p.println)
    }
  }
}
