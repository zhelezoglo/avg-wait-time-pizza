package pizza


import scala.collection.mutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._


object Cook extends App {

  def ordersAvgWaitTime(orders: Iterator[Order], numberOfOrders: Int): Future[Long] = Future {

    case class ProcessingReport(orderReport: OrderReport, ordersLeft: Int)
    case class OrderReport(curTime: Long, avgAcc: Long)

    val unprocessed = mutable.PriorityQueue[Order]()

    def processOrder(prevReport: OrderReport, order: Order): OrderReport = {
      val curTimeNow = if (prevReport.curTime < order.timeReceived) {
        order.timeReceived
      } else {
        prevReport.curTime
      }

      val waitTime = curTimeNow - order.timeReceived + order.timeToProcess
      val curTime = curTimeNow + order.timeToProcess

      OrderReport(curTime, prevReport.avgAcc + waitTime)
    }


    def combine(procReport: ProcessingReport, order: Order): ProcessingReport = {
      val currentTime = procReport.orderReport.curTime
      val ordersLeft = procReport.ordersLeft - 1
      val lastOrder = procReport.ordersLeft == 1
      if (lastOrder) {
        if (order.timeReceived > currentTime) {
          val procReportBeforeLast = unprocessed.dequeueAll.foldLeft(procReport.orderReport)(processOrder)
          ProcessingReport(processOrder(procReportBeforeLast, order), ordersLeft)
        } else {
          unprocessed.enqueue(order)
          val orderReport = unprocessed.dequeueAll.foldLeft(procReport.orderReport)(processOrder)
          ProcessingReport(orderReport, ordersLeft)
        }
      } else {
        if (order.timeReceived > currentTime) {
          val orderReport = unprocessed.dequeueAll.foldLeft(procReport.orderReport)(processOrder)
          unprocessed.enqueue(order)
          ProcessingReport(orderReport, ordersLeft)
        } else {
          unprocessed.enqueue(order)
          ProcessingReport(procReport.orderReport, ordersLeft)
        }
      }
    }

    val lastOrderReport = {
      val firstProcReport = {
        val firstOrder = orders.next()
        val firstOrderReport = processOrder(OrderReport(firstOrder.timeReceived, 0), firstOrder)
        ProcessingReport(firstOrderReport, numberOfOrders - 1)
      }
      val lastProcReport = orders.foldLeft(firstProcReport)(combine)
      lastProcReport.orderReport
    }

    lastOrderReport.avgAcc / numberOfOrders
  }
}
