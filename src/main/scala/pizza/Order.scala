package pizza

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.Source

case class Order(timeReceived: Long, timeToProcess: Long) extends Ordered[Order] {
  override def compare(that: Order): Int = {
    val compareProcTimes = that.timeToProcess.compareTo(this.timeToProcess)
    if (compareProcTimes == 0) {
      that.timeReceived.compareTo(this.timeReceived)
    } else {
      compareProcTimes
    }
  }
}


object Order {
  def apply(str: String): Order = str.split(" ") match {
    case Array(t: String, l: String) => Order(t.toInt, l.toInt)
  }

  def getOrders(fileName: String): Future[(Iterator[Order], Int)] = Future {
    val iterator = Source.fromURL(getClass.getResource(s"/pizza/$fileName")).getLines()
    val n = iterator.next().toInt
    (iterator.map(Order(_)), n)
  }
}
