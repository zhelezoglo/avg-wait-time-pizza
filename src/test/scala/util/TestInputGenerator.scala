package util

import org.scalatest.{FlatSpec, Matchers}


class TestInputGenerator extends FlatSpec with Matchers {
  def sorted(l: List[Long]) = l.view.zip(l.tail).forall(x => x._1 <= x._2)
  val iterator = InputGenerator.generateInput()

  "recTimes" should "not be decreasing" in {
    sorted(iterator.map(_.split(" ")(0).toLong).toList) should be(true)
  }
}
