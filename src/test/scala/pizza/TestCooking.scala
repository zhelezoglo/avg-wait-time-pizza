package pizza



import org.scalatest._
import org.scalatest.concurrent.AsyncAssertions
import pizza.Cook._
import pizza.Order._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Failure, Success}

class TestCooking extends FlatSpec with Matchers with ParallelTestExecution with AsyncAssertions {

  val fileNames = Array(
      ("orders0.txt", 9L),
      ("orders1.txt", 8L),
      ("orders2.txt", 7L),
      ("orders3.txt", 8L),
      ("example.txt", 16638357412815L)
  )

  it should "work for all inputs" in {
    fileNames.foreach {
      case (fileName: String, avg: Long) => {
        val waiter = new Waiter

        val averageWaitingTime = for {
          (orders, numberOfOrders) <- getOrders(fileName)
          res <- ordersAvgWaitTime(orders, numberOfOrders)
        } yield res

        averageWaitingTime.onComplete {
          case Success(result) => waiter(result should be(avg)); waiter.dismiss()
          case Failure(err) => println(err);
        }
        waiter.await(timeout(10 seconds))
      }
    }
  }


}
